# UE4 LibAV Video Player #

This project was made before a media player was available in UE4 and it was hard to playback any video on a surface. The project contains an Actor that creates a texture surface which uses to upload the video frames. This projects only reads the video frames and ignores the audio.

![ScreenShot00000_1.png](https://bitbucket.org/repo/GRaEdB/images/692146760-ScreenShot00000_1.png)