#include "UE4_LibAV_Player.h"
#include "VideoSurface.h"

AVFormatContext* format = nullptr;
AVCodec* codec = nullptr;
AVCodecContext* context = nullptr;
AVPacket packet;
int video_stream_index;
AVStream* video_stream;
AVFrame* frame;
AVFrame* rgb;

#pragma pack(push)
#pragma pack(1)
struct RGBColor 
{
	uint8_t r, g, b;
};
#pragma pack(pop)

UTexture2D* AVideoSurface::LoadTextureFromPath(const FString& Path)
{
	if (Path.IsEmpty()) return NULL;
	return Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, *(Path)));
}
 
AVideoSurface::AVideoSurface(const FObjectInitializer& init)
	: Super(init)
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> sm_plane(TEXT("StaticMesh'/Game/SM_TV.SM_TV'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> mat_brick(TEXT("Material'/Game/M_TV.M_TV'"));
	//static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> mat_copy(TEXT("MaterialInstanceConstant'/Game/M_TV_Inst.M_TV_Inst'"));
	//static ConstructorHelpers::FObjectFinder<UTexture2D> tex_brick(TEXT("Texture2D'/Game/brickwork-texture.brickwork-texture'"));
	
	Mesh = sm_plane.Object;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("PlaneComponent");
	MeshComponent->SetStaticMesh(Mesh);
	RootComponent = MeshComponent;

	BrickMaterial = mat_brick.Object;

	PrimaryActorTick.bCanEverTick = true;

	if (format == nullptr)
	{
		av_register_all();
		avcodec_register_all();
		FString path = FPaths::GameDir() + "Asset/video-tex.mp4";
		char *vid = TCHAR_TO_ANSI(*path);
		avformat_open_input(&format, vid, nullptr, nullptr);
		avformat_find_stream_info(format, nullptr);

		video_stream_index = av_find_best_stream(format, AVMEDIA_TYPE_VIDEO, -1, -1, &codec, 0);
		video_stream = format->streams[video_stream_index];
		context = video_stream->codec;
		avcodec_open2(context, codec, nullptr);

		frame = av_frame_alloc();
		rgb = av_frame_alloc();
		int rgb_size = avpicture_get_size(AV_PIX_FMT_BGR24, context->width, context->height);
		uint8_t* rgb_data = static_cast<uint8_t*>(av_malloc(rgb_size));
		avpicture_fill((AVPicture*)rgb, rgb_data, AV_PIX_FMT_BGR24, context->width, context->height);

		delay = video_stream->avg_frame_rate.den / video_stream->avg_frame_rate.num;

		av_init_packet(&packet);
	}

	UE_LOG(LogClass, Log, TEXT("******************************************************* LOG %d"), MYDEF);
}

void AVideoSurface::Tick(float DeltaSeconds)
{
	static float timer_acc = 0;
	static auto tex_brick = LoadTextureFromPath(TEXT("Texture2D'/Game/red.red'"));
	static unsigned char *data = new unsigned char[512 * 512 * 3];

	timer_acc += DeltaSeconds;

	if (timer_acc < delay)
	{
		return Super::Tick(DeltaSeconds);
	}

	timer_acc -= delay;

	int decoded = 0;
	while (format && !decoded && av_read_frame(format, &packet) == 0)
	{
		if (packet.stream_index != video_stream_index)
			continue;

		avcodec_decode_video2(context, frame, &decoded, &packet);
		if (decoded)
		{
			struct SwsContext * img_convert_ctx;
			img_convert_ctx = sws_getCachedContext(NULL, context->width, context->height, context->pix_fmt, context->width, context->height, AV_PIX_FMT_BGR24, SWS_BICUBIC, NULL, NULL, NULL);
			sws_scale(img_convert_ctx, frame->data, frame->linesize, 0, context->height, rgb->data, rgb->linesize);
			//stbi_write_bmp("out.bmp", context->width, context->height, 3, rgb->data[0]);

			av_free_packet(&packet);
			sws_freeContext(img_convert_ctx);
		}
	}

	FColor* MipData = static_cast<FColor*>(Texture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE));
	RGBColor* VideoData = (RGBColor*)rgb->data[0];
	int pixels = context->width * context->height;
	for (int i = 0; i < pixels; i++)
	{
		FColor& CurColor = MipData[i];
		RGBColor& VidColor = VideoData[i];
		CurColor.R = VidColor.b;
		CurColor.G = VidColor.g;
		CurColor.B = VidColor.r;
	}

	// Unlock the texturea
	Texture->PlatformData->Mips[0].BulkData.Unlock();
	Texture->UpdateResource();
	
	Super::Tick(DeltaSeconds);
}

void AVideoSurface::BeginPlay()
{
	av_seek_frame(format, video_stream_index, 0, AVSEEK_FLAG_BACKWARD);

}

void AVideoSurface::RerunConstructionScripts()
{
	Super::RerunConstructionScripts();
}

void AVideoSurface::OnConstruction(const FTransform& Transform)
{
	mat = UMaterialInstanceDynamic::Create(BrickMaterial, this);
	mat->SetVectorParameterValue(FName("Back Color"), FLinearColor(0, 0, 0));
	mat->SetVectorParameterValue(FName("Edges Color"), FLinearColor(.01, .01, .01));
	mat->SetVectorParameterValue(FName("Logo Color"), FLinearColor(.25, .25, .25));
	mat->SetScalarParameterValue(FName("Screen Roughness"), .6f);

	Texture = UTexture2D::CreateTransient(512, 512, PF_B8G8R8A8);
	mat->SetTextureParameterValue(FName("Texture"), Texture);

	MeshComponent->SetMaterial(0, mat);

	Super::OnConstruction(Transform);
}

void AVideoSurface::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	if (PropertyChangedEvent.MemberProperty->GetFName() == FName("Mesh"))
	{
		MeshComponent->SetStaticMesh(Mesh);
	}
	Super::PostEditChangeProperty(PropertyChangedEvent);
}


