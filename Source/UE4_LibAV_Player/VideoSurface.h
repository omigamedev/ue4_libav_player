

#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

#include "GameFramework/Actor.h"
#include "VideoSurface.generated.h"

/**
 * 
 */
UCLASS()
class AVideoSurface : public AActor
{
    GENERATED_BODY()
public:
	AVideoSurface(const FObjectInitializer& init);

    UPROPERTY(EditAnywhere, Category = Player)
    UStaticMesh* Mesh;
	
	UPROPERTY()
    UTexture2D* Texture;

    UStaticMeshComponent* MeshComponent;
    UMaterialInstanceDynamic* mat;
	UMaterial* BrickMaterial;
    float delay;

    UTexture2D* LoadTextureFromPath(const FString& Path);

    virtual void Tick(float DeltaSeconds) override;
    virtual void BeginPlay() override;
    virtual void RerunConstructionScripts() override;
    virtual void OnConstruction(const FTransform& Transform);
    virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
};
