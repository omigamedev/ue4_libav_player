

#include "UE4_LibAV_Player.h"
#include "UE4_LibAV_PlayerGameMode.h"
#include "UE4_LibAV_PlayerPlayerController.h"

AUE4_LibAV_PlayerGameMode::AUE4_LibAV_PlayerGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	PlayerControllerClass = AUE4_LibAV_PlayerPlayerController::StaticClass();
}


