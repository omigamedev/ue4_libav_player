

using UnrealBuildTool;
using System.IO;

public class UE4_LibAV_Player : ModuleRules
{
    private string ModulePath
    {
        get { return Path.GetDirectoryName(RulesCompiler.GetModuleFilename(this.GetType().Name)); }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "../../ThirdParty/")); }
    }

	public UE4_LibAV_Player(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
		PrivateDependencyModuleNames.AddRange(new string[] {  });

        PublicSystemIncludePaths.Add(ThirdPartyPath + "libav/include");
        string libs = ThirdPartyPath + "libav/lib/";

        PublicAdditionalLibraries.Add(libs + "avcodec.lib");
        PublicAdditionalLibraries.Add(libs + "avdevice.lib");
        PublicAdditionalLibraries.Add(libs + "avfilter.lib");
        PublicAdditionalLibraries.Add(libs + "avformat.lib");
        PublicAdditionalLibraries.Add(libs + "avresample.lib");
        PublicAdditionalLibraries.Add(libs + "avutil.lib");
        PublicAdditionalLibraries.Add(libs + "swscale.lib");
        Definitions.Add("MYDEF=13");
    }
}
