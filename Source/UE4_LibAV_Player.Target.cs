

using UnrealBuildTool;
using System.Collections.Generic;

public class UE4_LibAV_PlayerTarget : TargetRules
{
	public UE4_LibAV_PlayerTarget(TargetInfo Target)
	{
		Type = TargetType.Game;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "UE4_LibAV_Player" } );
	}
}
